FROM python:3.10-alpine

ADD flask /var/exp/flask
ENV PYTHONPATH="/var/exp/"
ENV FLASK_APP="app"

WORKDIR /var/exp/flask

EXPOSE 5000
RUN pip install flask \
	&& pip install gevent 
