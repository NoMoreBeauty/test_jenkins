from flask import render_template,Flask
from gevent import pywsgi

app = Flask(__name__)

@app.route('/hello/')
def hello():
    return render_template('hello.html')


if __name__ == '__main__':
    server = pywsgi.WSGIServer(('0.0.0.0', 5000), app)
    server.serve_forever()